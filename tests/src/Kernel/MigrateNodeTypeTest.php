<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\node\Entity\NodeType;

/**
 * Upgrade node types to node.type.*.yml.
 *
 * @group migrate_drupal_5
 */
class MigrateNodeTypeTest extends MigrateDrupal5TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node']);
    $this->executeMigration('d5_node_type');
  }

  /**
   * Tests Drupal 5 "page" node type to Drupal 8 migration.
   */
  public function testPageNodeType() {
    $id_map = $this->getMigration('d5_node_type')->getIdMap();
    $node_type= NodeType::load('page');
    $this->assertSame('page', $node_type->id(), 'Node type page loaded');
    $this->assertSame('Page', $node_type->get('name'));
    $this->assertFalse($node_type->displaySubmitted());
    $this->assertFalse($node_type->shouldCreateNewRevision());
    $this->assertSame(DRUPAL_OPTIONAL, $node_type->getPreviewMode());
    $this->assertSame($id_map->lookupDestinationIds(['page']), [['page']]);
    // Test we have a body field.
    $field_body = FieldConfig::loadByName('node', 'page', 'body');
    $this->assertSame('Body', $field_body->getLabel(), 'Body field was found.');
  }

  /**
   * Tests Drupal 5 "book" node type to Drupal 8 migration.
   */
  public function testBookNodeType() {
    $id_map = $this->getMigration('d5_node_type')->getIdMap();
    $node_type= NodeType::load('book');
    $this->assertSame('book', $node_type->id(), 'Node type book loaded');
    $this->assertSame('Book page', $node_type->get('name'));
    $this->assertTrue($node_type->displaySubmitted());
    $this->assertTrue($node_type->shouldCreateNewRevision());
    $this->assertSame(DRUPAL_OPTIONAL, $node_type->getPreviewMode());
    $this->assertSame($id_map->lookupDestinationIds(['book']), [['book']]);
    // Test we have a body field.
    $field_body = FieldConfig::loadByName('node', 'book', 'body');
    $this->assertSame('Book body label', $field_body->getLabel(), 'Body field was found.');
  }
}
