<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\filter\Entity\FilterFormat;
use Drupal\filter\FilterFormatInterface;

/**
 * Migrate filter formats.
 *
 * @group migrate_drupal_5
 */
class MigrateFilterFormatTest extends MigrateDrupal5TestBase {

  /**
   * @var \Drupal\filter\FilterPluginManager
   */
  protected $filterPluginManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->filterPluginManager = \Drupal::service('plugin.manager.filter');
    $this->executeMigration('d5_filter_format');
  }

  /**
   * Asserts various aspects of a filter format entity.
   *
   * @see \Drupal\Tests\filter\Kernel\Migrate\d7\MigrateFilterFormatTest::assertEntity
   *
   * @param string $id
   *   The format ID.
   * @param string $label
   *   The expected label of the format.
   * @param array $enabled_filters
   *   The expected filters in the format, keyed by ID with weight as values.
   *
   * @internal
   */
  protected function assertEntity(string $id, string $label, array $enabled_filters): void {
    /** @var \Drupal\filter\FilterFormatInterface $entity */
    $entity = FilterFormat::load($id);
    $this->assertInstanceOf(FilterFormatInterface::class, $entity);
    $this->assertSame($label, $entity->label());
    // get('filters') will return enabled filters only, not all of them.
    $this->assertSame(array_keys($enabled_filters), array_keys($entity->get('filters')));
    foreach ($entity->get('filters') as $filter_id => $filter) {
      $this->assertSame($filter['weight'], $enabled_filters[$filter_id]);
    }
  }

  /**
   * Test 'Empty input format' filter format.
   */
  public function testEmptyFilterFormat() {
    $this->assertEntity('empty_input_format', 'Empty input format', []);
  }

  /**
   * Test 'Filtered HTML' filter format.
   */
  public function testFilteredHtmlFilterFormat() {
    $this->assertEntity('filtered_html', 'Filtered HTML', ['filter_url' => 0, 'filter_html' => 1, 'filter_autop' => 2]);
    // filter_html
    $filter_plugin = $this->filterPluginManager->createInstance('filter_html');
    $filter_config = FilterFormat::load('filtered_html')->filters('filter_html')->getConfiguration();
    $this->assertSame($filter_plugin->getPluginDefinition()['settings']['allowed_html'], $filter_config['settings']['allowed_html']);
    $this->assertEquals(TRUE, $filter_config['settings']['filter_html_help']);
    // filter_url
    $filter_plugin = $this->filterPluginManager->createInstance('filter_url');
    $filter_config = FilterFormat::load('filtered_html')->filters('filter_url')->getConfiguration();
    $this->assertSame($filter_plugin->getPluginDefinition()['settings']['filter_url_length'], $filter_config['settings']['filter_url_length']);
  }

  /**
   * Test 'Full HTML' filter format.
   */
  public function testFullHtmlFilterFormat() {
    $this->assertEntity('full_html', 'Full HTML', ['filter_autop' => 1, 'filter_url' => 0]);
  }

  /**
   * Test 'PHP code' filter format.
   */
  public function testPhpCodeFilterFormat() {
    $this->assertEntity('php_code', 'PHP code', ['filter_html_escape' => 10]);
  }

  /**
   * Test 'Customized format' filter format.
   */
  public function testCustomizedFormatFilterFormat() {
    $this->assertEntity('customized_format', 'Customized format', ['filter_html' => 5, 'filter_autop' => 2, 'filter_html_escape' => -10, 'filter_url' => 0]);
    // filter_html
    $filter_config = FilterFormat::load('customized_format')->filters('filter_html')->getConfiguration();
    $this->assertSame('<a href hreflang> <em> <strong> <cite> <code> <ul type> <ol start type> <li> <dl> <dt> <dd> <test1> <test2>', $filter_config['settings']['allowed_html']);
    $this->assertEquals(TRUE, $filter_config['settings']['filter_html_help']);
    $this->assertEquals(TRUE, $filter_config['settings']['filter_html_nofollow']);
    // filter_url
    $filter_config = FilterFormat::load('customized_format')->filters('filter_url')->getConfiguration();
    $this->assertSame(123, $filter_config['settings']['filter_url_length']);
  }
}
