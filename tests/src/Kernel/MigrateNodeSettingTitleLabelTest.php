<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\Core\Field\Entity\BaseFieldOverride;

/**
 * @group migrate_drupal_5
 */
class MigrateNodeSettingTitleLabelTest extends MigrateDrupal5TestBase {

  protected static $modules = ['node', 'text', 'menu_ui'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node']);
    $this->executeMigration('d5_node_type');
    $this->executeMigration('d5_node_setting_title_label');
  }

  /**
   * Tests migration of the title field name for "book" node type.
   */
  public function testBookNodeTypeSticky() {
    $this->assertSame('Book title label', BaseFieldOverride::load('node.book.title')->getLabel());
  }
}
