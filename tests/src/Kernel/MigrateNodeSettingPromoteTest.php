<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\Core\Field\Entity\BaseFieldOverride;

/**
 * @group migrate_drupal_5
 */
class MigrateNodeSettingPromoteTest extends MigrateDrupal5TestBase {

  protected static $modules = ['node', 'text', 'menu_ui'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node']);
    $this->executeMigration('d5_node_type');
    $this->executeMigration('d5_node_setting_promote');
  }

  /**
   * Tests migration of the "promote" checkbox's settings for "page" node type.
   */
  public function testPageNodeTypePromote() {
    $this->assertSame(0, BaseFieldOverride::load('node.page.promote')->getDefaultValueLiteral()[0]['value']);
  }

  /**
   * Tests migration of the "promote" checkbox's settings for "book" node type.
   */
  public function testBookNodeTypePromote() {
    $this->assertSame(1, BaseFieldOverride::load('node.book.promote')->getDefaultValueLiteral()[0]['value']);
  }
}
