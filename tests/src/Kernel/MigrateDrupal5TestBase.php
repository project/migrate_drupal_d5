<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\Tests\migrate_drupal\Kernel\MigrateDrupalTestBase;
use Drupal\Tests\migrate_drupal\Traits\NodeMigrateTypeTestTrait;

/**
 * Base class for Drupal 5 migration tests.
 */
abstract class MigrateDrupal5TestBase extends MigrateDrupalTestBase {

  use NodeMigrateTypeTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'filter',
    'image',
    'link',
    'migrate_drupal_d5',
    'node',
    'options',
    'telephone',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->loadFixture($this->getFixtureFilePath());
  }

  /**
   * Gets the path to the fixture file.
   */
  protected function getFixtureFilePath() {
    return __DIR__ . '/../../fixtures/drupal5.php';
  }
}
