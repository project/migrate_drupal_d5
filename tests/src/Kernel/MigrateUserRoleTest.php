<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\filter\Entity\FilterFormat;
use Drupal\filter\FilterFormatInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Migrate user roles.
 *
 * @group migrate_drupal_5
 */
class MigrateUserRoleTest extends MigrateDrupal5TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->executeMigrations(['d5_filter_format', 'd5_user_role']);
  }

  /**
   * Asserts various aspects of a user role entity.
   *
   * @param string $id
   *   The role ID.
   * @param string $label
   *   The expected label of the role.
   * @param array $text_formats
   *   The expected text formats for user roles.
   * @param array $permissions
   *   The expected permissions for user roles.
   *
   * @internal
   */
  protected function assertEntity(string $id, string $label, array $text_formats = [], array $permissions = []): void {
    /** @var \Drupal\user\RoleInterface $entity */
    $entity = Role::load($id);
    $this->assertInstanceOf(RoleInterface::class, $entity);
    $this->assertSame($label, $entity->label());
    foreach ($text_formats as $format) {
      $this->assertTrue($entity->hasPermission("use text format $format"));
    }
    foreach ($permissions as $permission) {
      $this->assertTrue($entity->hasPermission($permission));
    }
  }

  /**
   * Test 'Foo' user role.
   */
  public function testFooUserRole() {
    $this->assertEntity('foo_role', 'Foo Role', ['php_code', 'customized_format'], ['administer menu', 'edit own forum content']);
  }

  /**
   * Test 'Bar' user role.
   */
  public function testBarUserRole() {
    $this->assertEntity('bar_role', 'Bar Role', ['full_html', 'customized_format'], ['administer menu', 'create forum content']);
  }

  /**
   * Test 'Baz' user role.
   */
  public function testBazUserRole() {
    $this->assertEntity('baz_role', 'Baz Role', ['php_code', 'customized_format'], ['administer menu', 'administer content with blog api', 'use PHP for settings']);
  }
}
