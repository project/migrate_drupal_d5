<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\Core\Field\Entity\BaseFieldOverride;

/**
 * @group migrate_drupal_5
 */
class MigrateNodeSettingStatusTest extends MigrateDrupal5TestBase {

  protected static $modules = ['node', 'text', 'menu_ui'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node']);
    $this->executeMigration('d5_node_type');
    $this->executeMigration('d5_node_setting_status');
  }

  /**
   * Tests migration of the "status" checkbox's settings for "page" node type.
   */
  public function testPageNodeTypeStatus() {
    $this->assertSame(0, BaseFieldOverride::load('node.page.status')->getDefaultValueLiteral()[0]['value']);
  }

  /**
   * Tests migration of the "status" checkbox's settings for "book" node type.
   */
  public function testBookNodeTypeStatus() {
    $this->assertSame(1, BaseFieldOverride::load('node.book.status')->getDefaultValueLiteral()[0]['value']);
  }
}
