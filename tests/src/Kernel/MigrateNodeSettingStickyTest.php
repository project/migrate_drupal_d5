<?php

namespace Drupal\Tests\migrate_drupal_d5\Kernel;

use Drupal\Core\Field\Entity\BaseFieldOverride;

/**
 * @group migrate_drupal_5
 */
class MigrateNodeSettingStickyTest extends MigrateDrupal5TestBase {

  protected static $modules = ['node', 'text', 'menu_ui'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node']);
    $this->executeMigration('d5_node_type');
    $this->executeMigration('d5_node_setting_sticky');
  }

  /**
   * Tests migration of the "sticky" checkbox's settings for "page" node type.
   */
  public function testPageNodeTypeSticky() {
    $this->assertSame(0, BaseFieldOverride::load('node.page.sticky')->getDefaultValueLiteral()[0]['value']);
  }

  /**
   * Tests migration of the "sticky" checkbox's settings for "book" node type.
   */
  public function testBookNodeTypeSticky() {
    $this->assertSame(1, BaseFieldOverride::load('node.book.sticky')->getDefaultValueLiteral()[0]['value']);
  }
}
