<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.3.13 db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('aggregator_category', array(
  'fields' => array(
    'cid' => array(
      'type' => 'serial',
      'not null' => TRUE,
      'size' => 'normal',
    ),
    'title' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '255',
      'default' => '',
    ),
    'description' => array(
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
    ),
    'block' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'tiny',
      'default' => '0',
    ),
  ),
  'primary key' => array(
    'cid',
  ),
  'unique keys' => array(
    'title' => array(
      'title',
    ),
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('aggregator_category')
->fields(array(
  'cid',
  'title',
  'description',
  'block',
))
->values(array(
  'cid' => '3',
  'title' => 'Drupal feed category',
  'description' => 'Drupal feed category description.',
  'block' => '5',
))
->values(array(
  'cid' => '4',
  'title' => 'Example feed category',
  'description' => '',
  'block' => '5',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}