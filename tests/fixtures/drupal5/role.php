<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.3.13 db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('role', array(
  'fields' => array(
    'rid' => array(
      'type' => 'serial',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'name' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '64',
      'default' => '',
    ),
  ),
  'primary key' => array(
    'rid',
  ),
  'unique keys' => array(
    'name' => array(
      'name',
    ),
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('role')
->fields(array(
  'rid',
  'name',
))
->values(array(
  'rid' => '1',
  'name' => 'anonymous user',
))
->values(array(
  'rid' => '2',
  'name' => 'authenticated user',
))
->values(array(
  'rid' => '4',
  'name' => 'Bar Role',
))
->values(array(
  'rid' => '6',
  'name' => 'Baz Role',
))
->values(array(
  'rid' => '3',
  'name' => 'Foo Role',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}