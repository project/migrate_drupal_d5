<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.3.13 db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('comments', array(
  'fields' => array(
    'cid' => array(
      'type' => 'serial',
      'not null' => TRUE,
      'size' => 'normal',
    ),
    'pid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'nid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'uid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'subject' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '64',
      'default' => '',
    ),
    'comment' => array(
      'type' => 'text',
      'not null' => TRUE,
      'size' => 'big',
    ),
    'hostname' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '128',
      'default' => '',
    ),
    'timestamp' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'score' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'medium',
      'default' => '0',
    ),
    'status' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'tiny',
      'default' => '0',
      'unsigned' => TRUE,
    ),
    'format' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'thread' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '255',
    ),
    'users' => array(
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ),
    'name' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '60',
    ),
    'mail' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '64',
    ),
    'homepage' => array(
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
    ),
  ),
  'primary key' => array(
    'cid',
  ),
  'indexes' => array(
    'lid' => array(
      'nid',
    ),
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('comments')
->fields(array(
  'cid',
  'pid',
  'nid',
  'uid',
  'subject',
  'comment',
  'hostname',
  'timestamp',
  'score',
  'status',
  'format',
  'thread',
  'users',
  'name',
  'mail',
  'homepage',
))
->values(array(
  'cid' => '1',
  'pid' => '0',
  'nid' => '8',
  'uid' => '2',
  'subject' => 'comment #1',
  'comment' => 'body of comment #1',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095280',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'spotiprepral',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '2',
  'pid' => '0',
  'nid' => '1',
  'uid' => '0',
  'subject' => 'comment #2',
  'comment' => 'body of comment #2',
  'hostname' => '192.168.1.2',
  'timestamp' => '1632095280',
  'score' => '0',
  'status' => '1',
  'format' => '0',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => '',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '4',
  'pid' => '0',
  'nid' => '3',
  'uid' => '0',
  'subject' => 'comment #4',
  'comment' => 'body of comment #4',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095280',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => '',
  'mail' => 'anon03@localhost.localdomain',
  'homepage' => '',
))
->values(array(
  'cid' => '5',
  'pid' => '4',
  'nid' => '3',
  'uid' => '1',
  'subject' => 'comment #5',
  'comment' => 'body of comment #5',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095340',
  'score' => '0',
  'status' => '1',
  'format' => '1',
  'thread' => '01.00/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'admin',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '6',
  'pid' => '0',
  'nid' => '12',
  'uid' => '7',
  'subject' => 'comment #6',
  'comment' => 'body of comment #6',
  'hostname' => '192.168.1.1',
  'timestamp' => '1632095340',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'swupipih',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '7',
  'pid' => '4',
  'nid' => '3',
  'uid' => '0',
  'subject' => 'comment #7',
  'comment' => 'body of comment #7',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095340',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01.01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => '',
  'mail' => 'anon01@example.com',
  'homepage' => 'http://example.com/',
))
->values(array(
  'cid' => '9',
  'pid' => '4',
  'nid' => '3',
  'uid' => '7',
  'subject' => 'comment #9',
  'comment' => 'body of comment #9',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095400',
  'score' => '0',
  'status' => '0',
  'format' => '0',
  'thread' => '01.02/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'swupipih',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '10',
  'pid' => '2',
  'nid' => '1',
  'uid' => '1',
  'subject' => 'comment #10',
  'comment' => 'body of comment #10',
  'hostname' => '192.168.1.3',
  'timestamp' => '1632095400',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01.00/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'admin',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '11',
  'pid' => '6',
  'nid' => '12',
  'uid' => '2',
  'subject' => 'comment #11',
  'comment' => 'body of comment #11',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095400',
  'score' => '0',
  'status' => '0',
  'format' => '4',
  'thread' => '01.00/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'spotiprepral',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '12',
  'pid' => '0',
  'nid' => '5',
  'uid' => '0',
  'subject' => 'comment #12',
  'comment' => 'body of comment #12',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095400',
  'score' => '0',
  'status' => '0',
  'format' => '4',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => '',
  'mail' => 'anon02@localhost',
  'homepage' => 'http://localhost/',
))
->values(array(
  'cid' => '13',
  'pid' => '4',
  'nid' => '3',
  'uid' => '3',
  'subject' => 'comment #13',
  'comment' => 'body of comment #13',
  'hostname' => '191.168.1.1',
  'timestamp' => '1632095460',
  'score' => '0',
  'status' => '1',
  'format' => '1',
  'thread' => '01.03/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'phamishajaru',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '14',
  'pid' => '0',
  'nid' => '14',
  'uid' => '1',
  'subject' => 'comment #14',
  'comment' => 'body of comment #14',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095460',
  'score' => '0',
  'status' => '0',
  'format' => '3',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'admin',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '17',
  'pid' => '1',
  'nid' => '8',
  'uid' => '3',
  'subject' => 'comment #17',
  'comment' => 'body of comment #17',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095520',
  'score' => '0',
  'status' => '1',
  'format' => '1',
  'thread' => '01.00/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'phamishajaru',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '18',
  'pid' => '0',
  'nid' => '15',
  'uid' => '1',
  'subject' => 'comment #18',
  'comment' => 'body of comment #18',
  'hostname' => '192.168.1.2',
  'timestamp' => '1632095580',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'admin',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '20',
  'pid' => '0',
  'nid' => '10',
  'uid' => '0',
  'subject' => 'comment #20',
  'comment' => 'body of comment #20',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632095640',
  'score' => '0',
  'status' => '0',
  'format' => '3',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => '',
  'mail' => '',
  'homepage' => 'http://localhost.localdomain/',
))
->values(array(
  'cid' => '21',
  'pid' => '9',
  'nid' => '3',
  'uid' => '1',
  'subject' => 'comment #21',
  'comment' => "body of comment #21\r\nSed quid sentiat, non videtis. Quid sequatur, quid repugnet, vident. Ergo id est convenienter naturae vivere, a natura discedere.",
  'hostname' => '192.168.1.1',
  'timestamp' => '1632170073',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01.02.00/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'admin',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '22',
  'pid' => '0',
  'nid' => '19',
  'uid' => '12',
  'subject' => 'comment #22',
  'comment' => 'body of comment #22',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632172500',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'vaswijimiwri',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '23',
  'pid' => '22',
  'nid' => '19',
  'uid' => '3',
  'subject' => 'comment #23',
  'comment' => 'body of comment #23',
  'hostname' => '192.168.1.1',
  'timestamp' => '1632172560',
  'score' => '0',
  'status' => '0',
  'format' => '4',
  'thread' => '01.00/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'phamishajaru',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '24',
  'pid' => '0',
  'nid' => '19',
  'uid' => '1',
  'subject' => 'comment #24',
  'comment' => 'body of comment #24',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632172645',
  'score' => '0',
  'status' => '0',
  'format' => '1',
  'thread' => '02/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'admin',
  'mail' => '',
  'homepage' => '',
))
->values(array(
  'cid' => '25',
  'pid' => '1',
  'nid' => '20',
  'uid' => '3',
  'subject' => 'comment #25',
  'comment' => 'body of comment #25',
  'hostname' => '127.0.0.1',
  'timestamp' => '1632172699',
  'score' => '0',
  'status' => '1',
  'format' => '1',
  'thread' => '01/',
  'users' => 'a:1:{i:0;i:0;}',
  'name' => 'phamishajaru',
  'mail' => '',
  'homepage' => '',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}