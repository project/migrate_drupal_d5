<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.3.13 db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('aggregator_category_item', array(
  'fields' => array(
    'iid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'cid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
  ),
  'primary key' => array(
    'iid',
    'cid',
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('aggregator_category_item')
->fields(array(
  'iid',
  'cid',
))
->values(array(
  'iid' => '1',
  'cid' => '4',
))
->values(array(
  'iid' => '2',
  'cid' => '4',
))
->values(array(
  'iid' => '3',
  'cid' => '4',
))
->values(array(
  'iid' => '4',
  'cid' => '4',
))
->values(array(
  'iid' => '5',
  'cid' => '4',
))
->values(array(
  'iid' => '6',
  'cid' => '4',
))
->values(array(
  'iid' => '7',
  'cid' => '4',
))
->values(array(
  'iid' => '8',
  'cid' => '4',
))
->values(array(
  'iid' => '9',
  'cid' => '4',
))
->values(array(
  'iid' => '10',
  'cid' => '4',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}