<?php

namespace Drupal\migrate_drupal_d5\Plugin\migrate\source\d5;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * The 'd5_profile_fields' source plugin.
 *
 * @MigrateSource(
 *   id = "d5_profile_field",
 *   source_module = "profile"
 * )
 */
class ProfileField extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('profile_fields', 'pf')
      ->fields('pf');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('Profile field ID.'),
      'title' => $this->t('Profile field label.'),
      'name' => $this->t('Profile field machine name.'),
      'explanation' => $this->t('Profile field description.'),
      'category' => $this->t('Profile field category.'),
      'page' => $this->t('Profile field page title.'),
      'type' => $this->t('Profile field type name.'),
      'weight' => $this->t('Profile field weight.'),
      'required' => $this->t('Profile field must enter a value.'),
      'register' => $this->t('Visible in user registration form.'),
      'visibility' => $this->t('Profile field visibility.'),
      'autocomplete' => $this->t('Form will auto-complete while user is typing.'),
      'options' => $this->t('Selection options.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['fid'] = [
      'type' => 'integer',
      'unsigned' => TRUE,
      'size' => 'big',
    ];
    return $ids;
  }
}
