<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal_d5\Plugin\migrate\source\d5\User.
 */

namespace Drupal\migrate_drupal_d5\Plugin\migrate\source\d5;

use Drupal\migrate\Row;
use Drupal\user\Plugin\migrate\source\d6\User as UserBase;

/**
 * Drupal 5 user source from database.
 *
 * @MigrateSource(
 *   id = "d5_user"
 * )
 */
class User extends UserBase {

  /**
   * {@inheritdoc}
   */
  protected function baseFields() {
    $fields = parent::baseFields();
    // The only difference between D5 and D6 is only D6 has signature_format.
    unset($fields['signature_format']);
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    if ($this->moduleExists('profile')) {
      $profile_fields = $this->select('profile_fields', 'pf')
        ->fields('pf', ['name', 'title'])
        ->execute();
      foreach ($profile_fields as $profile_field) {
        $fields[$profile_field['name']] = $this->t($profile_field['title']);
      }
    }
    return $fields;
  }

  /**
   * @inheritdoc
   */
  public function prepareRow(Row $row) {
    parent::prepareRow($row);
    foreach ($this->fields() as $name => $title) {
      if (strpos($name, 'profile_') === 0) {
        // profile field
        $uid = $row->getSourceProperty('uid');
        $profile_field = $this->select('profile_fields', 'pf')
          ->fields('pf', ['fid', 'type'])
          ->condition('name', $name)
          ->execute()
          ->fetchAssoc();
        $profile_value = $this->select('profile_values', 'pv')
          ->fields('pv', ['value'])
          ->condition('fid', $profile_field['fid'])
          ->condition('uid', $uid)
          ->execute()
          ->fetchField();
        if ($profile_value) {
          // process profile values
          switch ($profile_field['type']) {
            case 'textarea':
            case 'textfield':
            case 'selection':
            case 'url':
              $result = $profile_value;
              break;
            case 'checkbox':
              $result = (boolean) $profile_value;
              break;
            case 'date':
              $result = unserialize($profile_value);
              break;
            case 'list':
              $result = preg_split("/\r\n|\n|\r/", $profile_value);
              break;
          }
          $row->setSourceProperty($name, $result);
        }
      }
    }
  }
}
